# primasellar

This is audio application where we will hit api and call list of artist , albums and tracks 

Steps to follow for running the application

1) git clone git@bitbucket.org:krishcool688/primasellar.git
2) npm install
3) npm install --save axios 
4) npm start 

**** Brief

npm install will save and load all dependencies.

axios is used for fetching the data and its a promise based HTTP Client which is similar to ajax

npm start will run the development server which is sent to browser for use. 

