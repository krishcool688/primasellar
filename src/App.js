import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Navbar from './layouts/Navbar';
import Footer from "./layouts/Footer";
import Search from "./components/Search";
import Album from "./components/Album";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

class App extends Component {
  render() {
    return <div>
        <Navbar />
        <Route exact path="/" component={Search} />
        <Route exact path="/albums/:album_id" component={Album} />
        {/* <Search /> */}
        <Footer />
      </div>;
  }
}

export default App;
