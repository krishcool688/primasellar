import React, { Component } from "react";
import axios from "axios";
import Suggestions from "../components/Suggestions";


class Search extends Component {
  state = {
    query: '',
    results: []
  }

  handleInputChange = () => {    
    this.setState({
      query: "%" + this.search.value
    }, () => {
      if(this.state.query && this.state.query.length > 1) {
        if(this.state.query.length % 2 === 0) {
          this.getInfo();
        } else if (!this.state.query) {
        }
      } 
    })
  }
  

  getInfo = () => {        
    axios.get(`https://www.theaudiodb.com/api/v1/json/1/search.php?s=${this.state.query}&s=${this.state.query}`)    
      .then(({ data }) => {        
        this.setState({
          results: data    
        })
      })      
      .catch(function (error) {
        console.log(error);
      });       
  }  

  render() {
    // console.log(this.state.results)    
    return (            
      <div className="row mt-5">
        <div className="col-md-8 offset-md-2">
          <form>            
            <input
              placeholder="Search for Artists"
              ref={input => this.search = input}
              onChange={this.handleInputChange}
              className="form-control form-control-lg"
            />
             <Suggestions results={this.state.results} /> 
          </form>
        </div>
      </div>
    );
  }
}

export default Search;
