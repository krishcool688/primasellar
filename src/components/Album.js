import React, { Component } from "react";
import axios from "axios";
import { NavLink } from "react-router-dom";
import _ from "lodash";


class Album extends Component {
  state = {    
    album_data: [],
    showModal: false ,
    track_data: []
  };

  componentDidMount() {
    axios.get(`https://theaudiodb.com/api/v1/json/1/album.php?i=${this.props.match.params.album_id}`)
      .then(({ data }) => {        
        this.setState({
          album_data: data    
        })
      })      
      .catch(function (error) {
        console.log(error);
      }); 
  }  

  renderModal() {    
    // console.log(this.state.track_data.track)
      const grouped_album = _.groupBy(this.state.track_data.track && this.state.track_data.track, function(track_name) {
        return track_name.strAlbum;
      });       

      return <div class="modal fade show" id="exampleModalLong" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content track-modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">
                    <b>{_.keys(grouped_album)}</b> Track Details
                  </h5>
                </div>
            {_.values(grouped_album).map((item,key) => {
              return <div class="modal-body">              
                 {item.map((track,key) => {
                    return <p>{track.strTrack}</p>
                })}                        
              </div>
            })}                            
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal" onClick={() => this.setState(
                        { showModal: false }
                      )}>
                    Close
                  </button>
                </div>
              </div>;
          </div>
        </div>                     
    }


  handleModal = (id) => {
    this.setState({
      showModal: true
    })
    console.log(id)
    axios.get(`https://theaudiodb.com/api/v1/json/1/track.php?m=${id}`)
      .then(({ data }) => {        
        this.setState({
          track_data: data    
        })
      })      
      .catch(function (error) {
        console.log(error);
      });     
  }

  

  render() {    
    const grouped = _.groupBy(this.state.album_data.album, function(artist_name) {
      return artist_name.strArtist;
    });        

    const result = _.values(grouped).map((item,key)=> {
      return <div className="row">
        {item.map((art,k) => {
          return <div className="col-4 mb-3">
              <ul className="list-unstyled">
                <li className="media">
                  <img className="img-fluid" src={!_.isEmpty(art.strAlbumThumb) ? art.strAlbumThumb : "https://via.placeholder.com/150x150"} alt="Generic placeholder image" className="img-fluid thumb-img" />
                  <div className="media-body">
                    <h6 className="mt-0 mb-1 pl-2 pb-2">
                      {art.strAlbum}
                      &nbsp;
                      {/* <br />
                      <small>({art.strGenre})</small> */}
                    </h6>
                    <a onClick={() => this.handleModal(art.idAlbum)} className="modal_track_handler pl-2">
                      View
                    </a>
                    {this.state.showModal && this.renderModal()}

                    {/* <button type="button" class="btn btn-link" data-toggle="modal" data-target={key} onClick={this.showModal}>
                  Launch demo modal
                </button> */}
                  </div>
                </li>
                <hr />
              </ul>
            </div>
        })}
      </div>
    });
    // console.log(result)

    // console.log(this.state.album_data.album)
    return <div className="container">
        <div className="row">
          <div className="col-12">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <NavLink to="/">Back to Search</NavLink>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  {_.keys(grouped)}
                </li>
              </ol>
            </nav>
          </div>

          <div className="col-12">
            <ul class="list-unstyled">
              <li class="media">
                <img class="mr-3 img-fluid album-info-img" src="https://via.placeholder.com/200x150" alt="Generic placeholder image" />
                <div class="media-body">
                  <h5 class="mt-0 mb-1">{_.keys(grouped)}</h5>                
                </div>
              </li>
            </ul>
          </div>        

          <div className="col-12 mb-5">
            <h1>Albums<hr /></h1>
            {result}
          </div>
        </div>
      </div>      
  }
}

export default Album;
