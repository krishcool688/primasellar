import React from 'react';
import { NavLink } from 'react-router-dom';
import _ from "lodash";

const Suggestions = (props) => {  
  // console.log(props.results.artists);
  const options = props.results.artists && props.results.artists.map(r => (  
      <div className="col-12">
        <ul className="list-unstyled">
          <li className="media media-suggest-list mb-2" key={r.idArtist}>
            <img src={!_.isEmpty(r.strArtistLogo) ? r.strArtistLogo : "https://via.placeholder.com/64x64"} alt={r.strArtist} className="img-fluid suggest-artist-logo"/>
            <div className="media-body">
              <h5 className="mt-0 mb-1">{r.strArtist}</h5> 
              <NavLink to={"/albums/" + r.idArtist}>View Albumns</NavLink>             
            </div>
          </li>
        </ul>
      </div>      
    ));
  return <div className="row">{options}</div>
}

export default Suggestions;



// <li key={r.idArtist}>
      //   {r.strArtist}
      //   <NavLink to={"/albums/" + r.idArtist}>View Albumns</NavLink>
      // </li>